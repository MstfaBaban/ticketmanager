<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::post('/ticket', 'TicketController@store');

Route::get('/getticket/{name}', 'TicketController@getTicket');

Route::get('/ticket/{name?}', 'TicketController@show');


Route::group(['middleware' => 'App\Http\Middleware\AdditionMiddleware'], function()
{
    Route::post('/ticket/{name}/addition', 'TicketController@ِaddition');
});

Route::group(['middleware' => 'App\Http\Middleware\SubtractionMiddleware'], function()
{
    Route::post('/ticket/{name}/subtraction', 'TicketController@subtraction');
});

Route::group(['middleware' => 'App\Http\Middleware\MultiplicationMiddleware'], function()
{
    Route::post('/ticket/{name}/multiplication', 'TicketController@multiplication');
});

Route::group(['middleware' => 'App\Http\Middleware\DivisionMiddleware'], function()
{
    Route::post('/ticket/{name}/division', 'TicketController@division');
});

Route::group(['middleware' => 'App\Http\Middleware\SupervisorMiddleware'], function()
{
    Route::post('/ticket/{name}/supervisor', 'TicketController@supervisor');
});

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
    Route::get('/admin/users', 'UserController@index');

    Route::post('/admin/users/{id}', 'UserController@edit');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get(

    '/logout', function () {

        Auth::logout();

        return Redirect::to('/login');

    });
