<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('addition', function () {

    return auth()->user()->user_type == 'addition';
});
Broadcast::channel('subtraction', function () {

    return auth()->user()->user_type == 'subtraction';
});
Broadcast::channel('multiplication', function () {

    return auth()->user()->user_type == 'multiplication';
});
Broadcast::channel('division', function () {

    return auth()->user()->user_type == 'division';
});
Broadcast::channel('supervisor', function () {

    return auth()->user()->user_type == 'supervisor';
});

