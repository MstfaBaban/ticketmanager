<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SubtractionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_type != 'subtraction')
        {
        return new Response("unauthorized");
        }
        return $next($request);
    }
}
