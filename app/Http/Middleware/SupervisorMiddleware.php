<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SupervisorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_type != 'supervisor')
        {
            return new Response("unauthorized");
        }
        return $next($request);
    }
}
