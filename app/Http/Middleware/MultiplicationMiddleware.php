<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class MultiplicationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_type != 'multiplication')
        {
            return new Response("unauthorized");
        }
        return $next($request);
    }
}
