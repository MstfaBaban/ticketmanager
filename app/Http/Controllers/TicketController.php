<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use App\Events\ChangeRouteToAddition;
use App\Events\ChangeRouteToSubtraction;
use App\Events\ChangeRouteToDivision;
use App\Events\ChangeRouteToMultiplication;
use App\Events\ChangeRouteToSupervisor;


class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function show($name = null)
    {
        if($name!=null){
        $ticket = Ticket::where('name',$name)->orderBy('stage', 'asc')->get()->toJson(JSON_PRETTY_PRINT);
        }else{
        $ticket = Ticket::get()->first()->toJson(JSON_PRETTY_PRINT);

        }

        return $ticket;
    }

    public function getTicket($name)
    {
       
        $ticket = Ticket::where('name',$name)->where('history',0)->first()->toJson(JSON_PRETTY_PRINT);

        return $ticket;
    }

    // public function showView()
    // {
    //     event(new \App\Events\ChangeRotue("/maketicket"));
       
    //     return view('ticket');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ticket = Ticket::create([
            'name'     => $request->name,
            'user_id'  => auth()->id(),
            'stage'    => 0,
            'number'   => $request->number,
        ]);

        event(new ChangeRouteToAddition($request->name));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */


    public function ِaddition (Request $request,$name)
    {
        $ticket = Ticket::where('name',$name)->where('history',0)->first()->addition($request->number);

        event(new ChangeRouteToSubtraction($request->name));

        return $ticket;
    }

    public function subtraction (Request $request,$name)
    {
        $ticket = Ticket::where('name',$name)->where('history',0)->first()->subtraction($request->number);

        event(new ChangeRouteToMultiplication($request->name));

        return $ticket;
    }

    public function multiplication (Request $request,$name)
    {
        $ticket = Ticket::where('name',$name)->where('history',0)->first()->multiplication($request->number);

        event(new ChangeRouteToDivision($request->name));

        return $ticket;
    }

    public function division (Request $request,$name)
    {
        $ticket = Ticket::where('name',$name)->where('history',0)->first()->division($request->number);

        event(new ChangeRouteToSupervisor($request->name));

        return $ticket;
    }

    public function supervisor (Request $request,$name)
    {
        $ticket = Ticket::where('name',$name)->where('history',0)->first();

        $ticket->archive = 1;
        
        $ticket->save();

        return $ticket;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
