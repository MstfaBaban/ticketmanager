<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::get();

        return view('users',compact('users'));
    }

    public function edit(Request $request,$id)
    {
        $user = User::find($id);

        $user->name =$request->name;
        $user->email =$request->email;
        $user->user_type =$request->user_type;
        $user->save();
        return redirect()->back();
    }
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);

        $user->destroy();
        $user->save();
        return redirect()->back();
    }
}
