<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Ticket extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name', 'user_id', 'number','stage','history','archive',
    ];



    public function addition ($input)
    {
        $number = $this->attributes['number'];
        $stage = $this->attributes['stage'];
        $name = $this->attributes['name'];
        $history = $this->attributes['history'];
        $archive = $this->attributes['archive'];

        if($stage == 0 && $history == 0){

        $number = $number + $input;
        $stage = $stage + 1;
        
        $this->history = 1;
        $this->save();
        
        Ticket::create([
            'name'         => $name,
            'user_id'  => auth()->id(),
            'stage'    => $stage,
            'number'    => $number,
        ]);

        }
        else{return "Fail";}
    }

    public function subtraction ($input)
    {
        $number = $this->attributes['number'];
        $stage = $this->attributes['stage'];
        $name = $this->attributes['name'];
        $history = $this->attributes['history'];
        $archive = $this->attributes['archive'];
        
        if($stage == 1 && $history == 0){

        $number = $number - $input;
        $stage = $stage + 1;
        
        $this->history = 1;
        $this->save();

        Ticket::create([
            'name'         => $name,
            'user_id'  => auth()->id(),
            'stage'    => $stage,
            'number'    => $number,
        ]);
        }
        else{return "Fail";}
    }

    public function multiplication ($input)
    {
        $number = $this->attributes['number'];
        $stage = $this->attributes['stage'];
        $name = $this->attributes['name'];
        $history = $this->attributes['history'];
        $archive = $this->attributes['archive'];

        if($stage == 2 && $history == 0){

        $number = $number * $input;
        $stage = $stage + 1;
        
        $this->history = 1;
        $this->save();

        Ticket::create([
            'name'         => $name,
            'user_id'  => auth()->id(),
            'stage'    => $stage,
            'number'    => $number,
        ]);
        }
        else{return "Fail";}
    }
    
    public function division ($input)
    {
        $number = $this->attributes['number'];
        $stage = $this->attributes['stage'];
        $name = $this->attributes['name'];
        $history = $this->attributes['history'];
        $archive = $this->attributes['archive'];
        
        if($stage == 3 && $history == 0){
        
        $number = $number / $input;
        $stage = $stage + 1;
        
        $this->history = 1;
        $this->save();

        Ticket::create([
            'name'         => $name,
            'user_id'  => auth()->id(),
            'stage'    => $stage,
            'number'    => $number,
        ]);
        
        }
        else{return "Fail";}
    }
}
