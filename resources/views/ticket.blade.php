<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta  name="csrf-token" content="{{csrf_token()}}" >  
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    </head>
    <body>
        <div id="app">
            <ticket></ticket>
        </div>

        <script src="{{asset('js/app.js')}}"></script>

        <script>
            Echo.channel('home')
            .listen('NewMessage', (e)=>{
                console.log(e.message);
            });
        </script>

    </body>
</html>
