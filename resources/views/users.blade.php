@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users Component</div>

                <div class="card-body">
                
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">User Type</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                    </table>
                        @foreach ($users as $user)
                        <form action="{{url('/admin/users/'.$user->id )}}" method="POST"> 
                        <table class="table">
                        <tbody>
                            <tr>
                                @csrf
                                <th scope="row">{{$user->id}}</th>
                                <td><input type="text" name="name" value="{{$user->name}}"></td>
                                <td><input type="text" name="email" value="{{$user->email}}"></td>
                                <td><input type="text" name="user_type" value="{{$user->user_type}}"></td>
                                <td><button class="btn btn-info" type="submit" value="submit" name="submit">Edit</button></td>
                            </tr>
                        </tbody>
                    </table>
                </form>

                        @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
