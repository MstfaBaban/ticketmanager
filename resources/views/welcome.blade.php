<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
    <meta  name="csrf-token" content="{{csrf_token()}}" >  

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/app.css')}}">

    </head>
    <body>
        <div id="app">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">Task</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                        <li class="nav-item">
                            <router-link class="nav-link" to="/maketicket">Make Ticket </router-link>
                        </li>
                        <li class="nav-item">
                            <router-link class="nav-link" to="/ticket">Search Ticket</router-link>
                        </li>
                        </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item ">
                                    <a  class="nav-link " href="#" role="button" >
                                        {{ Auth::user()->name . "/" . Auth::user()->user_type}} 
                                    </a>
                                </li>
                                <li class="nav-item ">
                                    <a  class="nav-link" href="{{url('/logout')}}" role="button" >
                                        logout
                                    </a>
                                 </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                                @if(Auth::user()->user_type=='admin')
                                <li class="nav-item ">
                                    <a  class="nav-link" href="{{url('/admin/users')}}" role="button" >
                                        Admin
                                    </a>
                                 </li>
                                
                                @endif
                            @endguest
                            
                            
                        </ul>
                    </div>
                </nav>  
                <router-view></router-view>
            </div>
        </div>

        <script src="{{asset('js/app.js')}}"></script>

        <script>

            Echo.private('addition')
            .listen('ChangeRouteToAddition', (e)=>{
                router.push(e.route + '/?name=' +e.name);
            });
            Echo.private('subtraction')
            .listen('ChangeRouteToSubtraction', (e)=>{
                router.push(e.route + '/?name=' +e.name);
            });
            Echo.private('multiplication')
            .listen('ChangeRouteToMultiplication', (e)=>{
                router.push(e.route + '/?name=' +e.name);
            });
            Echo.private('division')
            .listen('ChangeRouteToDivision', (e)=>{
                router.push(e.route + '/?name=' +e.name);
            });
            Echo.private('supervisor')
            .listen('ChangeRouteToSupervisor', (e)=>{
                router.push(e.route + '/?name=' +e.name);
            });
        </script>

    </body>
</html>
